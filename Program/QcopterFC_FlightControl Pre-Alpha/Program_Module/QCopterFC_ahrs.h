/* #include "QCopterFC_ahrs.h" */

#ifndef __QCOPTERFC_AHRS_H
#define __QCOPTERFC_AHRS_H

#include "stm32f4xx.h"
/*=====================================================================================================*/
/*=====================================================================================================*/
void AHRS_Init( void );
void AHRS_Update( void );
/*=====================================================================================================*/
/*=====================================================================================================*/
#endif	
